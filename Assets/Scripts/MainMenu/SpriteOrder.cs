﻿using UnityEngine;

public class SpriteOrder : MonoBehaviour
{
    private SpriteRenderer Renderer;

    void Start()
    {
        Renderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        Renderer.sortingOrder = (int)(-transform.position.y * 100);
    }
}
