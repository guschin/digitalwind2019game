﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class AnyButtonController : MonoBehaviour
{
    public float Amplitude = 1f;

    private Vector3 StartPos;
    private float t = 0;
    private TextMeshProUGUI Text;

    private void Start()
    {
        StartPos = transform.position;
        Text = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        transform.position = new Vector3(transform.position.x, StartPos.y + Mathf.Sin(t) * Amplitude, transform.position.z);
        float Alpha = Mathf.Clamp01(Mathf.Abs(Mathf.Cos(t + 0.2f)) + 0.1f);
        Text.color = new Color(255f, 255f, 255f, Alpha);
        t = (t + Time.deltaTime) % (2 * Mathf.PI);

        if (Input.anyKeyDown)
        {
            SceneManager.LoadScene("LoadingScreen");
        }
    }
}
