﻿using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class RandomAmbience : MonoBehaviour
{
    public List<AudioClip> AmbientSounds;
    public float Delay = 130f;

    private AudioSource Source;
    private float t;

    private void Start()
    {
        Source = GetComponent<AudioSource>();
        t = Delay;
    }

    void Update()
    {
        if (t == 0f)
        {
            int Index = (int)(Random.value * AmbientSounds.Count);
            AudioClip Clip = AmbientSounds[Index];
            t = Clip.length + Random.value * Delay;
            Source.clip = Clip;
            Source.loop = false;
            Source.Play();
        }

        t = Mathf.Clamp(t - Time.deltaTime, 0f, t);
    }
}
