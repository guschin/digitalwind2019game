﻿namespace Game.Enums
{
    public enum Face { UP, DOWN, RIGHT, LEFT }
    public enum DragDropRegion {
        INVENTORY, FURNACE,
        FURNACE_RESULT, CRAFTING_RESULT,
        FAST_SLOT,
        OTHER, NULL
    }
    public enum InventoryScreen { INVENTORY, FURNACE, NULL }
    public enum HandState { HAND_FREE, HAND_PLACE, NULL }
    public enum SoundType {
        PICKUP, TREE_CHOP, ROCK_CHOP,
        FOREST_AMBIENCE, FOOTSTEP
    }
    public enum ChoppableType { TREE, ROCK }
    public enum InstrumentType { PICKAXE, AXE }
}
