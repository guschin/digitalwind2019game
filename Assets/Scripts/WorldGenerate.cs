﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class  WorldGenerate : MonoBehaviour
{
    public Transform BlocksContainer;
    public GameObject GroundSpritePrefab;

    [Serializable]
    public struct _GeneratorRange
    {
        public _GeneratorRange(float startX, float endX, float startY, float endY)
        {
            StartX = startX;
            EndX = endX;

            StartY = startY;
            EndY = endY;
        }
        
        public float StartX;
        public float EndX;

        public float StartY;
        public float EndY;
    }

    [Serializable]
    public struct _Pair
    {
        public List<GameObject> Prefabs;
        public int Count;
    }
    
    public List<_Pair> PickupSpawn;

    public void GenerateChunk(Vector3 ChunkPosition, float ChunkSide)
    {
        SpriteRenderer GroundSprite = Instantiate(GroundSpritePrefab, BlocksContainer).GetComponent<SpriteRenderer>();
        GroundSprite.transform.position = ChunkPosition * ChunkSide;
        GroundSprite.sortingLayerName = "Ground";
        GroundSprite.sortingOrder = (int) (ChunkPosition.x * ChunkSide + ChunkPosition.y);
        
        _GeneratorRange Range = new _GeneratorRange(
            ChunkPosition.x * ChunkSide,
            ChunkPosition.x * ChunkSide + ChunkSide,
            ChunkPosition.y * ChunkSide,
            ChunkPosition.y * ChunkSide + ChunkSide
        );
        
        foreach (var Pair in PickupSpawn)
        {
            for (int i = 0; i < Pair.Count; i++)
            {
                Vector3 Position = RandomPosition(Range);

                int Index = (int)(Pair.Prefabs.Count * Random.value);

                GameObject Item = Instantiate(Pair.Prefabs[Index], BlocksContainer);
                Item.transform.position = Position;
            }
        }
    }

    Vector3 RandomPosition(_GeneratorRange GeneratorRange)
    {
        return new Vector3(
            GeneratorRange.StartX + (GeneratorRange.EndX - GeneratorRange.StartX) * Random.value,
            GeneratorRange.StartY + (GeneratorRange.EndY - GeneratorRange.StartY) * Random.value
        );
    }
}
