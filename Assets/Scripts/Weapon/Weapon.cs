﻿using System.Collections;
using UnityEngine;
using Game.Enums;

public class Weapon : MonoBehaviour
{
    public GameObject Bullet;
    public Transform BulletSpawner;
    public float RateOfFire = 0.1f;
    public float BulletSpeed = 0.01f;
    public float Delay = 0f;

    [HideInInspector]public bool Ready {
        get => t == 0;
    }

    private float t = 0;
    
    IEnumerator SpawnStraightBullet()
    {
        yield return new WaitForSeconds(Delay);
        
        GameObject bullet = Instantiate(Bullet);
        bullet.transform.position = BulletSpawner.position;
        Bullet bulletComponent = bullet.GetComponent<Bullet>();
        bulletComponent.Speed = BulletSpeed;
        switch (gameObject.GetComponent<PlayerController>().Direction)
        {
            case Face.UP:
                bulletComponent.Direction = Vector2.up;
                break;

            case Face.DOWN:
                bulletComponent.Direction = Vector2.down;
                break;

            case Face.LEFT:
                bulletComponent.Direction = Vector2.left;
                break;

            case Face.RIGHT:
                bulletComponent.Direction = Vector2.right;
                break;

            default:
                Debug.Log("Default");
                break;
        }
        Destroy(bullet, 5f);
    }

    IEnumerator SpawnShotgunBullets()
    {
        yield return new WaitForSeconds(Delay);

        for (float i = 0.1f; i > -0.1f; i -= 0.04f)
        {
            GameObject bullet = Instantiate(Bullet);
            bullet.transform.position = BulletSpawner.position;
            Bullet bulletComponent = bullet.GetComponent<Bullet>();
            bulletComponent.Speed = BulletSpeed;

            switch (gameObject.GetComponent<PlayerController>().Direction)
            {
                case Face.UP:
                    bulletComponent.Direction = Vector2.up + new Vector2(i, 0f);
                    break;

                case Face.DOWN:
                    bulletComponent.Direction = Vector2.down + new Vector2(i, 0f);
                    break;

                case Face.LEFT:
                    bulletComponent.Direction = Vector2.left + new Vector2(0f, i);
                    break;

                case Face.RIGHT:
                    bulletComponent.Direction = Vector2.right + new Vector2(0f, i);
                    break;

                default:
                    Debug.Log("Default");
                    break;
            }
            Destroy(bullet, 5f);
        }
    }
    
    public void Shoot()
    {
        if (t == 0)
        {
            t = RateOfFire;
            StartCoroutine(SpawnStraightBullet());
            //SpawnShotgunBullets();
        }
    }

    private void Update()
    {
        t = Mathf.Clamp(t - Time.deltaTime, 0, RateOfFire);
    }
}
