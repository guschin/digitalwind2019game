﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
    [HideInInspector] public Vector2 Direction = new Vector2(0, 0);
    [HideInInspector] public float Speed = 0.01f;
    private SpriteRenderer Sprite;
    
    void Start()
    {
        Sprite = GetComponent<SpriteRenderer>();
    }
    
    void Update()
    {
        transform.Translate(Direction.normalized * Speed);
    }
}
