﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Recipe", menuName = "Recipes/Crafting Recipe")]
public class CraftingRecipe : ScriptableObject
{
    public int RecipeID;

    public ItemInfo Item1;
    public int Item1Amount;

    public ItemInfo Item2;
    public int Item2Amount;

    public ItemInfo Item3;
    public int Item3Amount;

    public ItemInfo ResultItem;
    public int ResultAmount;
}