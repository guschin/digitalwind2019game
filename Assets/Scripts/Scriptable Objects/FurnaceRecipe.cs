﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Recipe", menuName = "Recipes/Furnace Recipe")]
public class FurnaceRecipe : ScriptableObject
{
    public int RecipeID;

    public ItemInfo RefineItem;
    public int NeededRefineCount;

    public ItemInfo ResultItem;
    public int ResultCount;
}
