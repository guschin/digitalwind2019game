﻿using UnityEngine;
using System;

[CreateAssetMenu(fileName = "New Item", menuName = "Item")]
public class ItemInfo : ScriptableObject
{
    public int ItemId;
    public GameObject Prefab;
    public string Name;
    public Sprite ItemIcon;

    [Serializable]
    public struct _Tags
    {
        public bool IsFuel;
        public bool Placeable;
        public bool NotPickUppable;
        public bool Pickaxe;
        public bool Axe;
    }
    public _Tags Tags;
}