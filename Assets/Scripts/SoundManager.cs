﻿using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

using Game.Enums;

public class SoundManager : MonoBehaviour
{
    public List<AudioClip> PickupSounds;
    public List<AudioClip> TreeChopSounds;
    public List<AudioClip> RockChopSounds;
    public List<AudioClip> FootstepsSounds;
    public AudioClip ForestAmbience;

    private Dictionary<SoundType, int> Indexes = new Dictionary<SoundType, int>()
    {
        { SoundType.PICKUP, 0 },
        { SoundType.ROCK_CHOP, 2 },
        { SoundType.TREE_CHOP, 2 },
        { SoundType.FOREST_AMBIENCE, 3 },
        { SoundType.FOOTSTEP, 4 },
    };

    private void Start()
    {
        PlaySound(SoundType.FOREST_AMBIENCE, true);
    }

    // Возвращает длину проигрываемой композиции
    public float PlaySound(SoundType Sound, bool Repeat=false)
    {
        int Index;
        AudioSource Source = transform.GetChild(Indexes[Sound]).GetComponent<AudioSource>();
        switch (Sound)
        {
            case SoundType.PICKUP:
                Index = (int)(Random.value * PickupSounds.Count);
                Source.clip = PickupSounds[Index];
                break;

            case SoundType.TREE_CHOP:
                Index = (int)(Random.value * TreeChopSounds.Count);
                Source.clip = TreeChopSounds[Index];
                break;

            case SoundType.ROCK_CHOP:
                Index = (int)(Random.value * RockChopSounds.Count);
                Source.clip = RockChopSounds[Index];
                break;

            case SoundType.FOREST_AMBIENCE:
                Source.clip = ForestAmbience;
                break;

            case SoundType.FOOTSTEP:
                Index = (int)(Random.value * FootstepsSounds.Count);
                Source.clip = FootstepsSounds[Index];
                break;

            default:
                break;
        }
        Source.loop = Repeat;
        Source.Play();
        return Source.clip.length;
    }
}
