﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireColourSizeController : MonoBehaviour
{
    public RectTransform CopySize;

    private RectTransform SelfTransform;
    
    void Start()
    {
        SelfTransform = GetComponent<RectTransform>();
    }

    void Update()
    {
        SelfTransform.sizeDelta = new Vector2(SelfTransform.sizeDelta.x, CopySize.sizeDelta.y);
    }
}
