﻿using UnityEngine;

using Game.Enums;
using System.Collections.Generic;

public class FurnaceController : Interactible
{
    private UIInventory UIInventory;
    private GameObject FurnaceScreen;
    private FurnaceState State;

    private FurnaceRecipe CurrentRecipe;
    private int FurnaceID;

    private bool Hovered = false;
    private TintController TintController;

    private void Start()
    {
        TintController = GetComponent<TintController>();

        UIInventory = GameObject.FindGameObjectWithTag("UIInventory").GetComponent<UIInventory>();
        FurnaceID = GlobalState.CreateNewFurnace();
        State = GlobalState.Furnaces[FurnaceID];
    }

    public override void Interact()
    {
        UIInventory.LoadPlayerInventory(InventoryScreen.FURNACE);
        FurnaceScreen = GameObject.FindGameObjectWithTag("UIFurnaceScreen");

        UpdateUI();
        GlobalState.OpenedFurnaceID = State.FurnaceID;
    }

    public void UpdateUI()
    {
        if (GlobalState.OpenedFurnaceID != State.FurnaceID || GlobalState.InventoryOpened == false) return;

        FireMaskController FireIcon = FurnaceScreen.transform.GetChild(0).GetChild(1).GetComponent<FireMaskController>();
        UIInventoryItem FuelSlot = GameObject.FindGameObjectWithTag("UIFurnaceFuel").GetComponent<UIInventoryItem>();
        UIInventoryItem RefineSlot = GameObject.FindGameObjectWithTag("UIFurnaceRefine").GetComponent<UIInventoryItem>();
        UIInventoryItem ResultSlot = GameObject.FindGameObjectWithTag("UIFurnaceResult").GetComponent<UIInventoryItem>();

        FireIcon.SetValue(State.Value);

        if (State.FuelItem == null || State.FuelCount == 0) FuelSlot.SetDefaultValues();
        else FuelSlot.SetItem(State.FuelItem, State.FuelCount);

        if (State.RefineItem == null || State.RefineCount == 0) RefineSlot.SetDefaultValues();
        else RefineSlot.SetItem(State.RefineItem, State.RefineCount);

        if (State.ResultItem == null || State.ResultCount == 0) ResultSlot.SetDefaultValues();
        else ResultSlot.SetItem(State.ResultItem, State.ResultCount);
    }

    private void Update()
    {
        foreach (FurnaceRecipe recipe in GlobalState.FurnaceRecipes)
        {
            if (State.RefineItem == null || State.FuelItem == null) break;
            
            if (recipe.RefineItem.ItemId == State.RefineItem.ItemId &&
                State.RefineCount >= recipe.NeededRefineCount &&
                State.FuelCount != 0 && State.FuelItem.Tags.IsFuel && (
                    State.ResultItem == null || 
                    recipe.ResultItem.ItemId == State.ResultItem.ItemId
                ) && !State.Cooking)
            {
                if (!State.Cooking)
                    State.FuelCount -= 1;
                State.Cooking = true;
                CurrentRecipe = recipe;
                break;
            }
        }

        if (State.Value > 0 || State.Cooking)
        {
            State.Value += Time.deltaTime;
        }
        
        if (State.Value >= 1f)
        {
            if (State.Cooking)
            {
                State.ResultItem = CurrentRecipe.ResultItem;
                State.ResultCount += CurrentRecipe.ResultCount;

                State.RefineCount -= CurrentRecipe.NeededRefineCount;

                State.Cooking = false;
            }
            State.Value = 0f;
        }

        if (State.RefineCount <= 0)
        {
            State.RefineItem = null;
            State.RefineCount = 0;
            State.Cooking = false;
        }

        UpdateUI();
        TintUpdate();
    }

    public override void OnIteractibleHovered()
    {
        Hovered = true;
    }

    void TintUpdate()
    {
        if (Hovered)
        {
            TintController.EnableTint();
            Hovered = false;
        }
        else
        {
            TintController.DisableTint();
        }
    }

    public override void Interact(KeyValuePair<ItemInfo, int> ItemInHand)
    {
        Interact();
    }
}
