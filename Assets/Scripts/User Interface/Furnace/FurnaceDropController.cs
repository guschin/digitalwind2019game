﻿using UnityEngine;
using System.Collections.Generic;

using Game.Enums;
using Game.Structures;

public class FurnaceDropController : DropController
{
    private Inventory PlayerInventory;
    private UIInventory UIInventory;

    private void Start()
    {
        PlayerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        UIInventory = GameObject.FindGameObjectWithTag("UIInventory").GetComponent<UIInventory>();
    }

    public override void DropCallback()
    {
        var DraggedItem = DragController.DraggedItem.GetComponent<UIInventoryItem>();
        var FurnaceItem = GetComponent<UIInventoryItem>();
        FurnaceState FurnaceState = GlobalState.Furnaces[GlobalState.OpenedFurnaceID];

        switch (DragController.DragSource)
        {
            case DragDropRegion.INVENTORY:
                if (FurnaceItem.CurrentItem == null || FurnaceItem.CurrentItem.ItemId == -2)
                {
                    FurnaceItem.SetItem(DraggedItem.CurrentItem, DraggedItem.ItemCount);
                }
                else if (FurnaceItem.CurrentItem.ItemId == DraggedItem.CurrentItem.ItemId)
                {
                    FurnaceItem.SetItem(DraggedItem.CurrentItem, DraggedItem.ItemCount + FurnaceItem.ItemCount);
                }
                else
                {
                    PlayerInventory.AddItem(FurnaceItem.CurrentItem, FurnaceItem.ItemCount);
                    FurnaceItem.SetItem(DraggedItem.CurrentItem, DraggedItem.ItemCount);
                }

                if (gameObject.CompareTag("UIFurnaceFuel"))
                {
                    FurnaceState.FuelItem = FurnaceItem.CurrentItem;
                    FurnaceState.FuelCount = FurnaceItem.ItemCount;
                }
                else if (gameObject.CompareTag("UIFurnaceRefine"))
                {
                    FurnaceState.RefineItem = FurnaceItem.CurrentItem;
                    FurnaceState.RefineCount = FurnaceItem.ItemCount;
                }
                break;
            case DragDropRegion.FAST_SLOT:
                if (FurnaceItem.CurrentItem == null || FurnaceItem.CurrentItem.ItemId == -2)
                {
                    FurnaceItem.SetItem(DraggedItem.CurrentItem, DraggedItem.ItemCount);
                }
                else if (FurnaceItem.CurrentItem.ItemId == DraggedItem.CurrentItem.ItemId)
                {
                    FurnaceItem.SetItem(DraggedItem.CurrentItem, DraggedItem.ItemCount + FurnaceItem.ItemCount);
                }
                else
                {
                    DragController.Dropped = false;
                    return;
                }

                if (gameObject.CompareTag("UIFurnaceFuel"))
                {
                    FurnaceState.FuelItem = FurnaceItem.CurrentItem;
                    FurnaceState.FuelCount = FurnaceItem.ItemCount;
                }
                else if (gameObject.CompareTag("UIFurnaceRefine"))
                {
                    FurnaceState.RefineItem = FurnaceItem.CurrentItem;
                    FurnaceState.RefineCount = FurnaceItem.ItemCount;
                }
                break;
            case DragDropRegion.FURNACE:
                break;
            case DragDropRegion.OTHER:
                break;
            case DragDropRegion.NULL:
                break;
            default:
                break;
        }
    }
}
