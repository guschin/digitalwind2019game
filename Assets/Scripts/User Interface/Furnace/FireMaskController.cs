﻿using UnityEngine;
using UnityEngine.UI;

public class FireMaskController : MonoBehaviour
{
    private RectTransform SelfRectTransform;
    private RectTransform ParentRectTransform;
    [Range(0, 1)] public float Value = 0f;

    void Start()
    {
        SelfRectTransform = GetComponent<RectTransform>();
        ParentRectTransform = transform.parent.GetComponent<RectTransform>();
    }
    
    void Update()
    {
        SelfRectTransform.sizeDelta = new Vector2(
            SelfRectTransform.sizeDelta.x,
            ParentRectTransform.rect.y * (1 - Value) * 2
        );
    }

    public void SetValue(float Value)
    {
        this.Value = Value;
    }
}
