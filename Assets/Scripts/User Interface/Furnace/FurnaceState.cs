﻿public class FurnaceState
{
    public int FurnaceID = 0;

    public ItemInfo FuelItem = null;
    public int FuelCount = 0;

    public ItemInfo RefineItem = null;
    public int RefineCount = 0;

    public ItemInfo ResultItem = null;
    public int ResultCount = 0;

    public bool Cooking = false;
    public float Value = 0f;
}
