﻿using UnityEngine;
using UnityEngine.EventSystems;

using Game.Enums;
using Game.Structures;

public class FurnaceDragController : DragController
{
    private Inventory PlayerInventory;
    private UIInventory UIInventory;

    private void Start()
    {
        PlayerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        UIInventory = GameObject.FindGameObjectWithTag("UIInventory").GetComponent<UIInventory>();
    }

    public override void EndDragCallback()
    {
        var FurnaceState = GlobalState.Furnaces[GlobalState.OpenedFurnaceID];
        switch (DropSource)
        {
            case DragDropRegion.FAST_SLOT:
            case DragDropRegion.INVENTORY:
                GetComponent<UIInventoryItem>().SetDefaultValues();

                if (gameObject.CompareTag("UIFurnaceFuel"))
                {
                    FurnaceState.FuelItem = null;
                    FurnaceState.FuelCount = 0;
                }
                else if (gameObject.CompareTag("UIFurnaceRefine"))
                {
                    FurnaceState.RefineItem = null;
                    FurnaceState.RefineCount = 0;
                }
                else if (gameObject.CompareTag("UIFurnaceResult"))
                {
                    FurnaceState.ResultItem = null;
                    FurnaceState.ResultCount = 0;
                }
                break;
        }
    }
}
