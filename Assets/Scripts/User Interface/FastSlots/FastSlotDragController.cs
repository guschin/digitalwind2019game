﻿using UnityEngine;
using UnityEngine.EventSystems;

using Game.Enums;
using Game.Structures;

public class FastSlotDragController : DragController
{
    public override void EndDragCallback()
    {
        FastSlotsContainer FastSlots = GameObject.FindGameObjectWithTag("FastSlotsContainer").GetComponent<FastSlotsContainer>();
        FastSlots.SetItem(GetComponent<SlotIndex>().Index, null, 0);
        GetComponent<UIInventoryItem>().SetDefaultValues();
    }
}
