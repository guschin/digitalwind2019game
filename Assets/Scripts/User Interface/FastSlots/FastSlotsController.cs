﻿using UnityEngine;
using System.Collections.Generic;

using Game.Structures;

public class FastSlotsController : MonoBehaviour
{
    private GameObject FastSlotsContainer;
    private FastSlotsContainer FastSlots;
    private List<UIInventoryItem> UISlots;

    void Start()
    {
        FastSlotsContainer = GameObject.FindGameObjectWithTag("FastSlotsContainer");
        UISlots = new List<UIInventoryItem>();
    }

    void Update()
    {
        FastSlots = FastSlotsContainer.GetComponent<FastSlotsContainer>();
        if (UISlots.Count < FastSlots.ItemCount)
        {
            for (int i = 0; i < FastSlots.ItemCount; i++)
                UISlots.Add(transform.GetChild(i).GetComponentInChildren<UIInventoryItem>());
        }

        for (int i = 0; i < FastSlots.ItemCount; i++)
        {
            KeyValuePair<ItemInfo, int> Item;
            FastSlots.GetItemByIndex(i, out Item);
            if (Item.Key == null)
            {
                UISlots[i].SetDefaultValues();
            }
            else UISlots[i].SetItem(Item.Key, Item.Value);
        }
    }
}
