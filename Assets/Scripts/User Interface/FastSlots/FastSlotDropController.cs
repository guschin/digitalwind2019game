﻿using UnityEngine;

using Game.Enums;
using Game.Structures;

public class FastSlotDropController : DropController
{
    public static int DroppedSlotIndex;

    public override void DropCallback()
    {
        var UIInventoryItem = GetComponent<UIInventoryItem>();
        Inventory PlayerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        FastSlotsContainer FastSlots = GameObject.FindGameObjectWithTag("FastSlotsContainer").GetComponent<FastSlotsContainer>();
        var DraggedItem = DragController.DraggedItem.GetComponent<UIInventoryItem>();
        int Index = GetComponent<SlotIndex>().Index;

        switch (DragController.DragSource)
        {
            case DragDropRegion.INVENTORY:
                if (UIInventoryItem.CurrentItem == null || UIInventoryItem.CurrentItem.ItemId == -2)
                {
                    UIInventoryItem.SetItem(DraggedItem.CurrentItem, DraggedItem.ItemCount);
                }
                else if (UIInventoryItem.CurrentItem.ItemId == DraggedItem.CurrentItem.ItemId)
                {
                    UIInventoryItem.SetItem(DraggedItem.CurrentItem, DraggedItem.ItemCount + UIInventoryItem.ItemCount);
                }
                else
                {
                    PlayerInventory.AddItem(UIInventoryItem.CurrentItem, UIInventoryItem.ItemCount);
                    UIInventoryItem.SetItem(DraggedItem.CurrentItem, DraggedItem.ItemCount);
                }
                FastSlots.SetItem(Index, UIInventoryItem.CurrentItem, UIInventoryItem.ItemCount);
                break;

            case DragDropRegion.FURNACE_RESULT:
            case DragDropRegion.FURNACE:
                if (UIInventoryItem.CurrentItem == null || UIInventoryItem.CurrentItem.ItemId == -2)
                    UIInventoryItem.SetItem(DraggedItem.CurrentItem, DraggedItem.ItemCount);
                else if (UIInventoryItem.CurrentItem.ItemId == DraggedItem.CurrentItem.ItemId)
                    UIInventoryItem.SetItem(DraggedItem.CurrentItem, DraggedItem.ItemCount + UIInventoryItem.ItemCount);
                else
                {
                    DragController.Dropped = false;
                    return;
                }
                FastSlots.SetItem(Index, UIInventoryItem.CurrentItem, UIInventoryItem.ItemCount);
                break;

            case DragDropRegion.CRAFTING_RESULT:
                DroppedSlotIndex = Index;
                break;
        }
    }
}
