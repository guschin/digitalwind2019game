﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIInventoryItem : MonoBehaviour
{
    public ItemInfo DefaultItem;
    public ItemInfo CurrentItem { get; private set; } = null;

    public int ItemCount { get; private set; } = 0;

    private Image ItemImage;
    private TextMeshProUGUI ItemCountText;

    void Start()
    {
        ItemImage = transform.GetChild(0).GetComponent<Image>();
        ItemCountText = GetComponentInChildren<TextMeshProUGUI>();
    }
    
    void Update()
    {
        if (CurrentItem != null)
        {
            ItemImage.sprite = CurrentItem.ItemIcon;
            if (ItemCount == -1) ItemCountText.text = "";
            else ItemCountText.text = ItemCount.ToString();
        }
        else SetDefaultValues();
    }

    // Устанавливает значения в данном предмете как стандартные
    public void SetDefaultValues()
    {
        CurrentItem = DefaultItem;
        ItemCount = -1;
    }

    public void SetItem(ItemInfo info, int count)
    {
        CurrentItem = info;
        ItemCount = count;
    }
}
