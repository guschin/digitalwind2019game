﻿using UnityEngine;
using System.Collections.Generic;

using Game.Enums;
using Game.Structures;

public class UIInvItemDropController : DropController
{
    private Inventory PlayerInventory;
    private UIInventory UIInventory;

    private void Start()
    {
        PlayerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        UIInventory = GameObject.FindGameObjectWithTag("UIInventory").GetComponent<UIInventory>();
    }

    public override void DropCallback()
    {
        var Item = DragController.DraggedItem.GetComponent<UIInventoryItem>();
        switch (DragController.DragSource)
        {
            case DragDropRegion.INVENTORY:
                KeyValuePair<ItemInfo, int> Pair;
                if (PlayerInventory.GetItem(Item.CurrentItem, out Pair))
                {
                    PlayerInventory.PopItem(Pair.Key);
                    PlayerInventory.AddItem(Pair.Key, Pair.Value);
                }
                break;

            case DragDropRegion.FURNACE_RESULT:
            case DragDropRegion.FURNACE:
            case DragDropRegion.FAST_SLOT:
                PlayerInventory.AddItem(Item.CurrentItem, Item.ItemCount);
                UIInventory.UpdateInventory();
                break;

            case DragDropRegion.OTHER:
                break;
        }
    }
}
