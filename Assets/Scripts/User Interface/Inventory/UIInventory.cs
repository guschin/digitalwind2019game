﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using Game.Enums;
using Game.Structures;

public class UIInventory : MonoBehaviour
{
    public GameObject InventoryView;
    public GameObject InventoryContent;
    public GameObject UIItemPrefab;

    [Serializable]
    public struct _GridSize
    {
        public int Rows; public int Columns;
    }
    public _GridSize GridSize;

    public string ItemTextTemplate = "\"{0}\"x{1}";

    public Inventory PlayerInventory;

    public InventoryScreen CurrentScreen;
    private InventoryScreen _SavedScreen;
    private int _SavedFurnaceID;

    public void LoadPlayerInventory(InventoryScreen screen = InventoryScreen.INVENTORY)
    {
        GlobalState.ToggleInventory(true, GlobalState.OpenedFurnaceID);
        GameObject.FindGameObjectWithTag("UICanvas").transform.GetChild(1).gameObject.SetActive(false);
        CurrentScreen = screen;

        InventoryView.SetActive(true);
        for (int RowIndex = 0; RowIndex < GridSize.Rows; RowIndex++)
        {
            Transform Row = InventoryContent.transform.GetChild(RowIndex);
            for (int CellIndex = 0; CellIndex < GridSize.Columns; CellIndex++)
            {
                GameObject Cell = Row.GetChild(CellIndex).gameObject;
                UIInventoryItem UIItem = Cell.GetComponentInChildren<UIInventoryItem>();

                int Index = GridSize.Columns * RowIndex + CellIndex;
                KeyValuePair<ItemInfo, int> Item;
                bool Status = PlayerInventory.GetItemByIndex(Index, out Item);
                if (Status)
                {
                    UIItem.SetItem(Item.Key, Item.Value);
                }
                else UIItem.SetDefaultValues();
            }
        }

        Transform SideScreenView = InventoryView.transform.GetChild(1);
        Transform Toggle = InventoryView.transform.GetChild(2);
        switch (screen)
        {
            case InventoryScreen.INVENTORY:
                Toggle.gameObject.SetActive(_SavedScreen != InventoryScreen.INVENTORY && _SavedScreen != InventoryScreen.NULL);
                SideScreenView.GetChild(0).gameObject.SetActive(true);
                for (int i = 1; i < SideScreenView.childCount; i++)
                    SideScreenView.GetChild(i).gameObject.SetActive(false);

                var RecipesListController = GameObject.FindGameObjectWithTag("UIRecipesList").GetComponent<CraftingListController>();
                RecipesListController.LoadUI();

                break;
            case InventoryScreen.FURNACE:
                SideScreenView.GetChild(1).gameObject.SetActive(true);
                for (int i = 0; i < SideScreenView.childCount; i++)
                    if (i != 1) SideScreenView.GetChild(i).gameObject.SetActive(false);

                Toggle.gameObject.SetActive(true);
                Toggle.GetChild(1).GetComponentInChildren<TextMeshProUGUI>().text = "Костёр";
                break;
            default:
                break;
        }
    }

    public void UpdateInventory()
    {
        if (InventoryView.activeSelf)
        {
            LoadPlayerInventory(CurrentScreen);
        }
    }

    public void UnloadPlayerInventory()
    {
        GlobalState.ToggleInventory(false);
        InventoryView.SetActive(false);
        GameObject.FindGameObjectWithTag("UICanvas").transform.GetChild(1).gameObject.SetActive(true);
        _SavedScreen = InventoryScreen.NULL;
        var Card = GameObject.FindGameObjectWithTag("UIItemNameCard");
        if (Card != null) Destroy(Card);
    }

    public void LoadCraftingScreen()
    {
        if (CurrentScreen != InventoryScreen.INVENTORY)
        {
            _SavedScreen = CurrentScreen;
            _SavedFurnaceID = GlobalState.OpenedFurnaceID;
            GlobalState.OpenedFurnaceID = -1;
            LoadPlayerInventory();
        }
    }

    public void LoadSecondScreen()
    {
        GlobalState.OpenedFurnaceID = _SavedFurnaceID;
        LoadPlayerInventory(_SavedScreen);
    }
}
