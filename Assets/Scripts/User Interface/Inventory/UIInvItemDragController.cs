﻿using UnityEngine;
using UnityEngine.EventSystems;

using Game.Enums;
using Game.Structures;

public class UIInvItemDragController : DragController
{
    private Inventory PlayerInventory;
    private UIInventory UIInventory;

    private void Start()
    {
        PlayerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        UIInventory = GameObject.FindGameObjectWithTag("UIInventory").GetComponent<UIInventory>();
    }

    public override void EndDragCallback()
    {
        switch (DropSource)
        {
            case DragDropRegion.FURNACE:
            case DragDropRegion.FURNACE_RESULT:
            case DragDropRegion.FAST_SLOT:
                PlayerInventory.PopItem(DraggedItem.GetComponent<UIInventoryItem>().CurrentItem);
                DraggedItem.GetComponent<UIInventoryItem>().SetDefaultValues();
                UIInventory.UpdateInventory();
                break;

            case DragDropRegion.INVENTORY:
                UIInventory.UpdateInventory();
                break;
        }
    }
}
