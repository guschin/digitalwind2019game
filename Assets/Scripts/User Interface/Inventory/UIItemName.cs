﻿using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class UIItemName : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject NameCardPrefab;
    private static GameObject CurrentItem = null;
    private static GameObject Instance = null;

    public void OnPointerEnter(PointerEventData eventData)
    {
        ItemInfo Item = gameObject.GetComponent<UIInventoryItem>().CurrentItem;
        if (Item.ItemId != -2)
        {
            CurrentItem = gameObject;
            Instance = Instantiate(NameCardPrefab, GameObject.FindGameObjectWithTag("UICanvas").transform);
            Instance.GetComponentInChildren<TextMeshProUGUI>().text = Item.Name;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (Instance != null) Destroy(Instance.gameObject);
        Instance = null;
        CurrentItem = null;
    }
    
    void Update()
    {
        if (Instance != null)
            Instance.transform.position = Input.mousePosition + new Vector3(5f, 5f);
    }
}
