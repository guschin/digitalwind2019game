﻿using UnityEngine;

public class CraftingListController : MonoBehaviour
{
    public GameObject CraftingRecipePrefab;
    private Transform RecipesListContent;

    public void LoadUI()
    {
        RecipesListContent = transform.GetChild(0).GetChild(0).transform;
        for (int i = 0; i < RecipesListContent.childCount; i++)
        {
            Destroy(RecipesListContent.GetChild(i).gameObject);
        }
            

        foreach (CraftingRecipe recipe in GlobalState.CraftingRecipes)
        {
            var UIRecipe = Instantiate(CraftingRecipePrefab, RecipesListContent);
            UIRecipe.GetComponent<RecipesSetter>().Recipe = recipe;
            UIRecipe.name = recipe.ResultItem.name + "Recipe";
            var UIItem = UIRecipe.GetComponentInChildren<UIInventoryItem>();
            UIItem.SetItem(recipe.ResultItem, recipe.ResultAmount);
        }
    }
}
