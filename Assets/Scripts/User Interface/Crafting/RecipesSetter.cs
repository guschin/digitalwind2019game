﻿using UnityEngine;

public class RecipesSetter : MonoBehaviour
{
    public CraftingRecipe Recipe;

    public void SetRecipe()
    {
        Transform RecipeIngridients = GameObject.FindGameObjectWithTag("UICraftingIngridients").transform;

        var FirstSlot = RecipeIngridients.GetChild(0).GetChild(0).GetComponent<UIInventoryItem>();
        var SecondSlot = RecipeIngridients.GetChild(1).GetChild(0).GetComponent<UIInventoryItem>();
        var ThirdSlot = RecipeIngridients.GetChild(2).GetChild(0).GetComponent<UIInventoryItem>();

        FirstSlot.SetItem(Recipe.Item1, Recipe.Item1Amount);
        SecondSlot.SetItem(Recipe.Item2, Recipe.Item2Amount);
        ThirdSlot.SetItem(Recipe.Item3, Recipe.Item3Amount);

        var RecipeResult = GameObject.FindGameObjectWithTag("UIRecipeResult").GetComponentInChildren<UIInventoryItem>();
        RecipeResult.SetItem(Recipe.ResultItem, Recipe.ResultAmount);
    }
}
