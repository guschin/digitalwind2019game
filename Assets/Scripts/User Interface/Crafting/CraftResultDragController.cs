﻿using System.Collections.Generic;
using UnityEngine;

using Game.Enums;
using Game.Structures;

public class CraftResultDragController : DragController
{
    private List<UIInventoryItem> Slots;
    private Inventory PlayerInventory;
    private UIInventory UIInventory;

    private void Start()
    {
        PlayerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        UIInventory = GameObject.FindGameObjectWithTag("UIInventory").GetComponent<UIInventory>();
    }

    private bool CanCraft()
    {
        for (int i = 0; i < 3; i++)
        {
            Debug.LogFormat("Checking {0} ingridient...", i);
            if (Slots[i].CurrentItem != null && Slots[i].CurrentItem.ItemId != -2)
            {
                KeyValuePair<ItemInfo, int> FirstItemInventory;
                if (!PlayerInventory.GetItem(Slots[i].CurrentItem, out FirstItemInventory))
                {
                    Debug.LogFormat("{0} ingridient not found in inventory", i);
                    return false;
                }
                else if (FirstItemInventory.Value < Slots[i].ItemCount)
                {
                    Debug.LogFormat("Not enough of {0} ingridient", i);
                    return false;
                }
            }
        }

        return true;
    }

    public override void EndDragCallback()
    {
        FastSlotsContainer FastSlots = GameObject.FindGameObjectWithTag("FastSlotsContainer").GetComponent<FastSlotsContainer>();
        Transform RecipeIngridients = GameObject.FindGameObjectWithTag("UICraftingIngridients").transform;

        Slots = new List<UIInventoryItem>();
        for (int i = 0; i < 3; i++)
            Slots.Add(RecipeIngridients.GetChild(i).GetComponentInChildren<UIInventoryItem>());
        
        var ResultItem = GetComponent<UIInventoryItem>();
        
        if (CanCraft())
        {
            

            switch (DropSource)
            {
                case DragDropRegion.INVENTORY:
                    PlayerInventory.RemoveItem(Slots[0].CurrentItem, Slots[0].ItemCount);
                    PlayerInventory.RemoveItem(Slots[1].CurrentItem, Slots[1].ItemCount);
                    PlayerInventory.RemoveItem(Slots[2].CurrentItem, Slots[2].ItemCount);

                    PlayerInventory.AddItem(ResultItem.CurrentItem, ResultItem.ItemCount);

                    UIInventory.UpdateInventory();
                    break;

                case DragDropRegion.FAST_SLOT:
                    KeyValuePair<ItemInfo, int> Item;
                    FastSlots.GetItemByIndex(FastSlotDropController.DroppedSlotIndex, out Item);

                    if (Item.Key == null || Item.Key.ItemId == -2)
                        FastSlots.SetItem(FastSlotDropController.DroppedSlotIndex, ResultItem.CurrentItem, ResultItem.ItemCount);
                    else if (Item.Key.ItemId == ResultItem.CurrentItem.ItemId)
                        FastSlots.SetItem(FastSlotDropController.DroppedSlotIndex, ResultItem.CurrentItem, ResultItem.ItemCount + Item.Value);
                    else
                    {
                        Dropped = false;
                        return;
                    }

                    PlayerInventory.RemoveItem(Slots[0].CurrentItem, Slots[0].ItemCount);
                    PlayerInventory.RemoveItem(Slots[1].CurrentItem, Slots[1].ItemCount);
                    PlayerInventory.RemoveItem(Slots[2].CurrentItem, Slots[2].ItemCount);

                    UIInventory.UpdateInventory();
                    break;
            }
        }
        
        Slots[0].SetDefaultValues();
        Slots[1].SetDefaultValues();
        Slots[2].SetDefaultValues();

        ResultItem.SetDefaultValues();
    }
}
