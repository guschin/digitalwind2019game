﻿using UnityEngine;
using System.Collections.Generic;
using TMPro;

public class HelpScreen : MonoBehaviour
{
    public List<string> Texts;
    private int CurrentScreen = 0;
    private TextMeshProUGUI Text;

    // Use this for initialization
    void Start()
    {
        Text = transform.GetChild(1).GetComponent<TextMeshProUGUI>();
    }
    
    void Update()
    {
        for (int i = 0; i < transform.GetChild(0).childCount; i++)
        {
            if (i != CurrentScreen) transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
            else transform.GetChild(0).GetChild(i).gameObject.SetActive(true);
        }

        transform.GetChild(3).gameObject.SetActive(CurrentScreen != 0);
        transform.GetChild(4).gameObject.SetActive(CurrentScreen != Texts.Count - 1);
        Text.text = Texts[CurrentScreen];
    }

    public void NextScreen()
    {
        if (CurrentScreen < Texts.Count - 1)
            CurrentScreen += 1;
    }

    public void PreviousScreen()
    {
        if (CurrentScreen > 0)
            CurrentScreen -= 1;
    }

    public void CloseScreen()
    {
        Destroy(gameObject);
    }
}
