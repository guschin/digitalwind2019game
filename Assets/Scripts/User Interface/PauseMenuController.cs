﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuController : MonoBehaviour
{
    void Start()
    {
        transform.GetChild(2).gameObject.SetActive(false);
    }

    public void OpenMenu()
    {
        GlobalState.PauseMenuOpened = true;
        transform.GetChild(2).gameObject.SetActive(true);
    }

    public void CloseMenu()
    {
        GlobalState.PauseMenuOpened = false;
        transform.GetChild(2).gameObject.SetActive(false);
    }

    public void LoadMainMenu()
    {
        GlobalState.UnloadGlobalState();
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }
}
