﻿using UnityEngine;
using UnityEngine.EventSystems;

using Game.Enums;

public abstract class DropController : MonoBehaviour, IDropHandler
{
    public DragDropRegion ThisDropSource;

    public void OnDrop(PointerEventData eventData)
    {
        if (DragController.DraggedItem == null) return;

        DragController.Dropped = true;
        DragController.DropSource = ThisDropSource;

        DropCallback();
    }

    public abstract void DropCallback();
}
