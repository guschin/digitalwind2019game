﻿using UnityEngine;
using UnityEngine.EventSystems;

using Game.Enums;

public abstract class DragController : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public DragDropRegion ThisDragSource;

    public static GameObject DraggedItem;
    public static bool Dropped = false;

    public static DragDropRegion DragSource;
    public static DragDropRegion DropSource;

    protected Vector3 StartPosition;
    protected Transform OriginalParent;

    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        var InventoryItem = GetComponent<UIInventoryItem>();
        if (DraggedItem == null &&
            InventoryItem.CurrentItem != null &&
            InventoryItem.CurrentItem.ItemId != -2)
        {
            DragSource = ThisDragSource;
            DraggedItem = gameObject;
            OriginalParent = gameObject.transform.parent;
            gameObject.transform.SetParent(GameObject.FindGameObjectWithTag("UICanvas").transform);

            StartPosition = transform.position;
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        if (DraggedItem != null)
            DraggedItem.transform.position = Input.mousePosition;
    }

    public virtual void OnEndDrag(PointerEventData eventData)
    {
        if (DraggedItem == null) return;
        DraggedItem.transform.SetParent(OriginalParent);
        OriginalParent = null;

        GetComponent<CanvasGroup>().blocksRaycasts = true;

        if (Dropped) EndDragCallback();

        DraggedItem.transform.position = StartPosition;
        DraggedItem = null;
        Dropped = false;

        DragSource = DragDropRegion.NULL;
        DropSource = DragDropRegion.NULL;
    }

    public abstract void EndDragCallback();
}
