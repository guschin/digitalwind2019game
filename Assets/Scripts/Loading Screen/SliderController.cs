﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class SliderController : MonoBehaviour
{
    public Slider LoadingSlider;
    private bool Loading = false;

    private void Update()
    {
        if (!Loading)
        {
            StartCoroutine(AsyncLoadForest());
            Loading = true;
        }
    }

    IEnumerator AsyncLoadForest()
    {
        AsyncOperation Operation = SceneManager.LoadSceneAsync("ForestScene");

        while (!Operation.isDone)
        {
            LoadingSlider.value = Operation.progress / 0.9f;
            yield return null;
        }
    }
}
