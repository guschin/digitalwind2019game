﻿using UnityEngine;

using Game.Enums;

public class ItemPlacerController : MonoBehaviour
{
    public float PlaceRadius = 1f;
    
    private GameObject CurrentItem = null;
    private SpriteRenderer ItemSprite = null;
    private Callback callback = null;
    
    void Update()
    {
        float Magnitude = 0f;
        if (CurrentItem != null)
        {
            GlobalState.HandState = HandState.HAND_PLACE;

            Vector3 Position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Position.z = 0;
            CurrentItem.transform.position = Position;

            Magnitude = (Position - transform.position).magnitude;
            if (Magnitude <= PlaceRadius)
                ItemSprite.color = Color.green;
            else ItemSprite.color = Color.red;

        } else GlobalState.HandState = HandState.HAND_FREE;

        float Fire = Input.GetAxis("Fire1");

        if (Fire == 1 && GlobalState.HandState == HandState.HAND_PLACE && GlobalState.PlaceDelay == 0f
            && Magnitude > 0 && Magnitude <= PlaceRadius)
        {
            ItemPlace();
        }
        GlobalState.PlaceDelay = Mathf.Clamp(GlobalState.PlaceDelay - Time.deltaTime, 0f, GlobalState.PlaceDelay);
    }

    public void SetItemPlacer(GameObject Item, Callback callback)
    {
        SpriteRenderer Sprite = Item.GetComponent<SpriteRenderer>();
        if (Sprite != null)
        {
            CurrentItem = Item;
            CurrentItem.GetComponent<Item>().ItemInfo.Tags.NotPickUppable = true;
            ItemSprite = Sprite;
            GlobalState.HandState = HandState.HAND_PLACE;
            this.callback = callback;
        }
    }

    public void Clear(bool DestroyItem = true)
    {
        if (CurrentItem != null)
        {
            if (DestroyItem) Destroy(CurrentItem);
            CurrentItem = null;
            ItemSprite = null;
        }
        GlobalState.HandState = HandState.HAND_FREE;
    }

    public void ItemPlace()
    {
        if (CurrentItem != null && ItemSprite != null)
        {
            GlobalState.HandState = HandState.HAND_FREE;
            ItemSprite.color = Color.white;
            CurrentItem.GetComponent<Item>().ItemInfo.Tags.NotPickUppable = true;
            CurrentItem = null;
            ItemSprite = null;
            GlobalState.PlaceDelay = 0.1f;
            callback();
        }
    }
}
