﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

using Game.Enums;
using Game.Structures;

public class FastSlotsScroller : MonoBehaviour
{
    public Sprite ChosenSlot;
    public Sprite IdleSlot;

    public int CurrentSlot { get; private set; } = 0;
    private FastSlotsContainer FastSlots;
    private List<Image> UISlots;
    
    void Start()
    {
        FastSlots = GameObject.FindGameObjectWithTag("FastSlotsContainer").GetComponent<FastSlotsContainer>();
        UISlots = new List<Image>();
    }

    public void DecreaseItem()
    {
        FastSlots.ChangeValueByIndex(CurrentSlot, -1);
        KeyValuePair<ItemInfo, int> Item;
        if (FastSlots.GetItemByIndex(CurrentSlot, out Item) && Item.Value == 0)
        {
            FastSlots.SetItem(CurrentSlot, null, 0);
            var Placer = GameObject.FindGameObjectWithTag("Player").GetComponent<ItemPlacerController>();
            Placer.Clear();
        }
    }
    
    void Update()
    {
        if (UISlots.Count < FastSlots.ItemCount)
        {
            var UIContainer = GameObject.FindGameObjectWithTag("UIIngameFastSlots").transform;
            for (int i = 0; i < FastSlots.ItemCount; i++)
                UISlots.Add(UIContainer.GetChild(i).GetComponent<Image>());
        }

        float ScrollWheel = Input.GetAxis("Mouse ScrollWheel");
        int SlotsCount = (int) -(ScrollWheel / 0.1f);

        
        CurrentSlot = (CurrentSlot + SlotsCount) % UISlots.Count;
        if (CurrentSlot < 0) CurrentSlot = FastSlots.ItemCount + CurrentSlot;
        UISlots[CurrentSlot].sprite = ChosenSlot;
        for (int i = 0; i < FastSlots.ItemCount; i++)
            if (i != CurrentSlot) UISlots[i].sprite = IdleSlot;

        var Placer = GameObject.FindGameObjectWithTag("Player").GetComponent<ItemPlacerController>();
        if (Mathf.Abs(SlotsCount) > 0)
            Placer.Clear();

        KeyValuePair<ItemInfo, int> Item;
        if (FastSlots.GetItemByIndex(CurrentSlot, out Item) && Item.Key != null)
        {
            if (Item.Key.Tags.Placeable)
            {
                if (GlobalState.HandState == HandState.HAND_FREE)
                {
                    Callback callback = DecreaseItem;
                    Placer.SetItemPlacer(Instantiate(Item.Key.Prefab), callback);
                }
            }
            else Placer.Clear();
        }
    }
    
    public void SetSlot(int Index)
    {
        if (Index < FastSlots.ItemCount)
            CurrentSlot = Index;
    }
}
