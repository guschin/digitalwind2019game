﻿using UnityEngine;
using Game.Enums;

public class PlayerController : MonoBehaviour
{
    public float Speed = 0.1f;
    public Animator PlayerAnimator;

    [HideInInspector] public Face Direction;
    private Face AnimationDirection;

    private Rigidbody2D PlayerRigidbody;
    private Transform CameraTransform;

    public UIInventory UIInventory;

    private Vector3 PreviousMove = new Vector3();
    private float SoundDelay = 0f;
    private SoundManager SoundManager;
    private PauseMenuController PauseController;

    void Start()
    {
        PauseController = GameObject.FindGameObjectWithTag("UICanvas").GetComponent<PauseMenuController>();
        SoundManager = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        PlayerRigidbody = GetComponent<Rigidbody2D>();
        CameraTransform = Camera.main.transform;
        PlayerAnimator = GetComponentInChildren<Animator>();

        Direction = Face.RIGHT;
        AnimationDirection = Direction;
    }

    void SetMoveAnimation(Vector3 move)
    {
        PlayerAnimator.SetInteger("SpeedX", (int) (move.x * 100));
        PlayerAnimator.SetInteger("SpeedY", (int) (move.y * 100));
        PlayerAnimator.SetBool("IsWalking", move.magnitude != 0);
        
        if (move.x > 0)
        {
            Direction = Face.RIGHT;
        }
        if (move.x < 0)
        {
            Direction = Face.LEFT;
        }
        if (move.y > 0)
        {
            Direction = Face.UP;
        }
        if (move.y < 0)
        {
            Direction = Face.DOWN;
        }

        if (AnimationDirection != Direction || PreviousMove.sqrMagnitude == 0 && move.sqrMagnitude != 0)
        {
            switch (Direction)
            {
                case Face.UP:
                    AnimationDirection = Face.UP;
                    PlayerAnimator.SetTrigger("WalkUp");
                    break;

                case Face.DOWN:
                    AnimationDirection = Face.DOWN;
                    PlayerAnimator.SetTrigger("WalkDown");
                    break;

                case Face.LEFT:
                    AnimationDirection = Face.LEFT;
                    PlayerAnimator.SetTrigger("WalkLeft");
                    break;

                case Face.RIGHT:
                    AnimationDirection = Face.RIGHT;
                    PlayerAnimator.SetTrigger("WalkRight");
                    break;
            }
        }

        PreviousMove = move;
    }

    void ToggleInventory()
    {
        if (GlobalState.PauseMenuOpened) return;

        if (GlobalState.InventoryOpened) UIInventory.UnloadPlayerInventory();
        else UIInventory.LoadPlayerInventory();
    }

    void TogglePauseMenu()
    {
        if (GlobalState.InventoryOpened) UIInventory.UnloadPlayerInventory();
        else
        {
            if (GlobalState.PauseMenuOpened) PauseController.CloseMenu();
            else PauseController.OpenMenu();
        }
    }

    void PlayFootstep()
    {
        if (SoundDelay == 0f)
        {
            float Length = SoundManager.PlaySound(SoundType.FOOTSTEP);
            SoundDelay = Length;
        }
    }

    void Update()
    {
        float Horizontal = Input.GetAxis("Horizontal");
        float Vertical = Input.GetAxis("Vertical");
        
        Vector3 Move = new Vector3(Horizontal, Vertical).normalized * Time.deltaTime * Speed;
        PlayerRigidbody.position = transform.position + Move;

        SetMoveAnimation(Move);
        if (Move.magnitude > 0) PlayFootstep();
        SoundDelay = Mathf.Clamp(SoundDelay - Time.deltaTime, 0f, SoundDelay);

        bool Escape = Input.GetKeyDown(KeyCode.Escape);
        if (Escape)
        {
            TogglePauseMenu();
        }

        bool Tab = Input.GetKeyDown(KeyCode.I);
        if (Tab) ToggleInventory();
    }
}
