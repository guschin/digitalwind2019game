﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public bool FollowPlayer = true;
    public Vector3 Offset;
    public bool MoveSmoothly = false;
    public float InterpolationSpeed = 0.1f;

    private Transform PlayerTransform;
    private Transform CameraTransform;

    private void Start()
    {
        CameraTransform = Camera.main.transform;
        PlayerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void FixedUpdate()
    {
        if (FollowPlayer && !MoveSmoothly)
        {
            CameraTransform.position = transform.position + Offset;
        }

        if (FollowPlayer && MoveSmoothly)
        {
            Vector3 DesiredPosition = transform.position + Offset;

            Vector3 SmoothPosition = Vector3.Lerp(
                CameraTransform.position,
                DesiredPosition,
                InterpolationSpeed
            );
            CameraTransform.position = SmoothPosition;

        }
    }
}