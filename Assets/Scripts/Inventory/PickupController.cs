﻿using UnityEngine;

using Game.Enums;
using Game.Structures;

public class PickupController : MonoBehaviour
{
    public Inventory PlayerInventory;
    public UIInventory UIInventory;

    private SoundManager SoundManager;

    private void Start()
    {
        SoundManager = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Item item = other.gameObject.GetComponent<Item>();
        if (item != null && !item.ItemInfo.Tags.NotPickUppable)
        {
            PlayerInventory.AddItem(item.ItemInfo);
            Destroy(other.gameObject);
            UIInventory.UpdateInventory();
            SoundManager.PlaySound(SoundType.PICKUP);
        }
    }
}
