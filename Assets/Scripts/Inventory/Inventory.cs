﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Structures
{
    public class Inventory : MonoBehaviour
    {
        public int ItemCount { get; private set; } = 0;

        private List<ItemInfo> ItemsList = new List<ItemInfo>();
        private List<int> ItemsCounts = new List<int>();

        public void AddItem(ItemInfo Info, int Count = 1)
        {
            if (Contains(Info))
            {
                ItemsCounts[IndexOf(Info)] += Count;
            }
            else
            {
                ItemsList.Add(Info);
                ItemsCounts.Add(Count);
                ItemCount += 1;
            }
        }

        public void RemoveItem(ItemInfo Info, int Count = 1)
        {
            if (Contains(Info))
            {
                ItemsCounts[IndexOf(Info)] -= Count;
                if (ItemsCounts[IndexOf(Info)] <= 0)
                {
                    PopItem(Info);
                }
            }
        }

        // Возвращает статус выполнения операции (удачно или не удачно)
        public bool SetItem(int Index, ItemInfo Info, int Count = 1)
        {
            if (Index < ItemCount)
            {
                ItemsList[Index] = Info;
                ItemsCounts[Index] = Count;
                return true;
            }
            else return false;
        }

        public void PopItem(ItemInfo Info)
        {
            if (Contains(Info))
            {
                int Index = IndexOf(Info);
                ItemsList.RemoveAt(Index);
                ItemsCounts.RemoveAt(Index);
                ItemCount -= 1;
            }
        }

        // Возвращает статус выполнения операции (удачно или не удачно)
        public bool GetItemByIndex(int Index, out KeyValuePair<ItemInfo, int> Item)
        {
            if (Index >= ItemCount)
            {
                Item = new KeyValuePair<ItemInfo, int>();
                return false;
            }
            else
            {
                Item = new KeyValuePair<ItemInfo, int>(ItemsList[Index], ItemsCounts[Index]);
                return true;
            }
        }

        // Возвращает статус выполнения операции (удачно или не удачно)
        public bool GetItem(ItemInfo Info, out KeyValuePair<ItemInfo, int> Item)
        {
            if (Contains(Info))
            {
                return GetItemByIndex(IndexOf(Info), out Item);
            }
            else
            {
                Item = new KeyValuePair<ItemInfo, int>();
                return false;
            }
        }

        public int IndexOf(ItemInfo Info)
        {
            for (int i = 0; i < ItemCount; i++)
            {
                if (ItemsList[i].ItemId == Info.ItemId) return i;
            }
            return -1;
        }

        public bool Contains(ItemInfo Info)
        {
            return IndexOf(Info) != -1;
        }
    }
}
