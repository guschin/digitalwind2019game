﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Structures
{
    public class FastSlotsContainer : MonoBehaviour
    {
        public int InitializeCount;
        public int ItemCount { get; private set; } = 0;

        private List<ItemInfo> ItemsList = new List<ItemInfo>();
        private List<int> ItemsCounts = new List<int>();

        private void Start()
        {
            ItemCount = InitializeCount;
            
            for (int i = 0; i < ItemCount; i++)
            {
                ItemsList.Add(null);
                ItemsCounts.Add(0);
            }
        }

        // Возвращает статус выполнения операции (удачно или не удачно)
        public bool ChangeValue(ItemInfo Info, int Delta)
        {
            if (Contains(Info))
            {
                int Index = IndexOf(Info);
                if (ItemsCounts[Index] + Delta > 0)
                {
                    ItemsCounts[Index] += Delta;
                    return true;
                }
                else if (ItemsCounts[Index] + Delta == 0)
                {
                    ItemsList[Index] = null;
                    ItemsCounts[Index] = 0;
                    return true;
                }
                else return false;
            }
            else return false;
        }

        // Возвращает статус выполнения операции (удачно или не удачно)
        public bool ChangeValueByIndex(int Index, int Delta)
        {
            if (Index < ItemCount)
            {
                if (ItemsCounts[Index] + Delta >= 0)
                {
                    ItemsCounts[Index] += Delta;
                    return true;
                }
                else if (ItemsCounts[Index] + Delta == 0)
                {
                    ItemsList[Index] = null;
                    ItemsCounts[Index] = 0;
                    return true;
                }
                else return false;
            }
            else return false;
        }

        // Возвращает статус выполнения операции (удачно или не удачно)
        public bool SetItem(int Index, ItemInfo Info, int Count = 1)
        {
            if (Index < ItemCount)
            {
                ItemsList[Index] = Info;
                ItemsCounts[Index] = Count;
                return true;
            }
            else return false;
        }

        // Возвращает статус выполнения операции (удачно или не удачно)
        public bool ReplaceItem(ItemInfo Info, int Count = 1)
        {
            if (Contains(Info))
            {
                return SetItem(IndexOf(Info), Info, Count);
            }
            else return false;
        }

        // Возвращает статус выполнения операции (удачно или не удачно)
        public bool ClearItem(ItemInfo Info)
        {
            if (Contains(Info)) return ClearItemByIndex(IndexOf(Info));
            else return false;
        }

        // Возвращает статус выполнения операции (удачно или не удачно)
        public bool ClearItemByIndex(int Index)
        {
            if (Index < ItemCount)
            {
                ItemsList[Index] = null;
                ItemsCounts[Index] = 0;
                return true;
            }
            else return false;
        }

        // Возвращает статус выполнения операции (удачно или не удачно)
        public bool GetItem(ItemInfo Info, out KeyValuePair<ItemInfo, int> Item)
        {
            if (Contains(Info))
            {
                return GetItemByIndex(IndexOf(Info), out Item);
            }
            else
            {
                Item = new KeyValuePair<ItemInfo, int>();
                return false;
            }
        }

        // Возвращает статус выполнения операции (удачно или не удачно)
        public bool GetItemByIndex(int Index, out KeyValuePair<ItemInfo, int> Item)
        {
            if (Index >= ItemCount)
            {
                Item = new KeyValuePair<ItemInfo, int>();
                return false;
            }
            else
            {
                Item = new KeyValuePair<ItemInfo, int>(ItemsList[Index], ItemsCounts[Index]);
                return true;
            }
        }

        public int IndexOf(ItemInfo Info)
        {
            for (int i = 0; i < ItemCount; i++)
            {
                if (ItemsList[i].ItemId == Info.ItemId) return i;
            }
            return -1;
        }

        public bool Contains(ItemInfo Info)
        {
            return IndexOf(Info) != -1;
        }
    }
}
