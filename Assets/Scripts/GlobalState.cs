﻿using UnityEngine;
using System.Collections.Generic;

using Game.Enums;

public class GlobalState : MonoBehaviour
{
    public static List<ItemInfo> ItemsList = new List<ItemInfo>();
    public static List<FurnaceRecipe> FurnaceRecipes = new List<FurnaceRecipe>();
    public static List<CraftingRecipe> CraftingRecipes = new List<CraftingRecipe>();

    public static Dictionary<int, FurnaceState> Furnaces = new Dictionary<int, FurnaceState>();
    public static List<KeyValuePair<int, int>> LoadedChunks = new List<KeyValuePair<int, int>>();

    public static bool PauseMenuOpened { get; set; } = false;
    public static bool InventoryOpened { get; private set; } = false;
    public static int OpenedFurnaceID = -1;
    public static HandState HandState = HandState.HAND_FREE;
    public static float PlaceDelay = 0f;

    private static int FurnaceMaxID = 0;
    
    void Start()
    {
        foreach (ItemInfo item in Resources.LoadAll<ItemInfo>("Items"))
        {
            ItemsList.Add(item);
        }
        
        foreach (FurnaceRecipe recipe in Resources.LoadAll<FurnaceRecipe>("Recipes/Furnace"))
            FurnaceRecipes.Add(recipe);

        foreach (CraftingRecipe recipe in Resources.LoadAll<CraftingRecipe>("Recipes/Crafting"))
            CraftingRecipes.Add(recipe);
    }

    // Возвращает ID новой печи
    public static int CreateNewFurnace()
    {
        Furnaces[FurnaceMaxID] = new FurnaceState();
        Furnaces[FurnaceMaxID].FurnaceID = FurnaceMaxID;

        return FurnaceMaxID++;
    }

    public static void ToggleInventory(bool InventoryState, int FurnaceID = -1)
    {
        InventoryOpened = InventoryState;
        OpenedFurnaceID = FurnaceID;
    }

    public static void UnloadGlobalState()
    {
        PauseMenuOpened = false;
        ToggleInventory(false);
        HandState = HandState.HAND_FREE;
        FurnaceMaxID = 0;
        PlaceDelay = 0;
        Furnaces.Clear();
        ItemsList.Clear();
        FurnaceRecipes.Clear();
        CraftingRecipes.Clear();
    }
}
