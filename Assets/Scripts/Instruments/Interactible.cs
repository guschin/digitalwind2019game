﻿using UnityEngine;
using System.Collections.Generic;

public abstract class Interactible : MonoBehaviour
{
    public abstract void Interact();
    public abstract void Interact(KeyValuePair<ItemInfo, int> ItemInHand);
    public virtual void OnIteractibleHovered() { }
}
