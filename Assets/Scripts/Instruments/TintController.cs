﻿using UnityEngine;

[RequireComponent(typeof(SpriteMask))]
public class TintController : MonoBehaviour
{
    private SpriteMask Mask;
    private SpriteRenderer Sprite;
    private bool Enabled = false;
    private SpriteRenderer TintSprite;
    
    void Start()
    {
        Sprite = GetComponent<SpriteRenderer>();

        Mask = GetComponent<SpriteMask>();
        Mask.sprite = Sprite.sprite;
        Mask.enabled = Enabled;

        TintSprite = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    public void EnableTint()
    {
        Mask.enabled = true;
        TintSprite.gameObject.SetActive(true);
        int SelfOrder = Sprite.sortingOrder;
        TintSprite.sortingOrder = SelfOrder + 1;
        Enabled = true;
    }

    public void DisableTint()
    {
        TintSprite.gameObject.SetActive(false);
        Mask.enabled = false;
        Enabled = false;
    }

    public void ToggleTint()
    {
        if (Enabled) DisableTint();
        else EnableTint();
    }

    private void Update()
    {
        if (Enabled)
        {
            Mask.sprite = Sprite.sprite;
            TintSprite.sortingLayerID = Sprite.sortingLayerID;
        }
    }
}
