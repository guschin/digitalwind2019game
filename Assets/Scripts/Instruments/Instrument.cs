using UnityEngine;

using Game.Enums;

public class Instrument : MonoBehaviour
{
    public float Capacity;
    public InstrumentType InstrumentType;
}