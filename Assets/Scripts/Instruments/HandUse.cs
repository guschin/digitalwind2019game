﻿using UnityEngine;
using System.Collections.Generic;

using Game.Enums;
using Game.Structures;

public class HandUse : MonoBehaviour
{
    public float UseDelay = 1f;
    public float UseRadius = 1f;
    public Vector3 Offset;

    private float t = 0f;
    private Interactible HoveredInteractible = null;
    private FastSlotsContainer FastSlots;

    // Находится ли курсор над обьектом, с которым можно взаимодействовать
    bool Raycast()
    {
        RaycastHit2D hit2D = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit2D)
        {
            GameObject HitObject = hit2D.transform.gameObject;
            Interactible choppable = HitObject.GetComponent<Interactible>();
            if (choppable != null) HoveredInteractible = choppable;
            return choppable != null;
        }
        return false;
    }

    private void Start()
    {
        FastSlots = GameObject.FindGameObjectWithTag("FastSlotsContainer").GetComponent<FastSlotsContainer>();
    }

    void Update()
    {
        if (GlobalState.HandState != HandState.HAND_FREE) return;

        FastSlotsScroller Scroller = GameObject.FindGameObjectWithTag("Player").GetComponent<FastSlotsScroller>();

        bool Hovered = Raycast();
        if (Hovered && HoveredInteractible != null) HoveredInteractible.OnIteractibleHovered();

        float Fire = Input.GetAxis("Fire1");
        if (Fire == 1 && t == 0 && 
            !GlobalState.InventoryOpened && GlobalState.HandState == HandState.HAND_FREE
            && GlobalState.PlaceDelay == 0f && Hovered && HoveredInteractible != null &&
            (transform.position + Offset - HoveredInteractible.transform.position).magnitude < UseRadius)
        {
            t = UseDelay;
            KeyValuePair<ItemInfo, int> Item;
            if (FastSlots.GetItemByIndex(Scroller.CurrentSlot, out Item) && Item.Key != null)
                HoveredInteractible.Interact(Item);
            else HoveredInteractible.Interact();
        }

        t = Mathf.Clamp(t - Time.deltaTime, 0f, UseDelay);
        //Debug.Log(GlobalState.HandState);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position + Offset, UseRadius);
    }
}
