﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

using Game.Enums;

public class Choppable : Interactible
{
    public ChoppableType ChoppableType;
    public float ChopHealth = 5f;
    public float DropRange = 1f;

    [Serializable]
    public struct ChoppableItem
    {
        public GameObject Item;
        // Количество вещей, которые выпадут со 100% вероятностью
        public int EnsuredCount;

        [Range(0, 1)] public float Probability;
        // Максимальное количество вещей, которые выпадут с вероятностью Probability
        public int ProbableCount;
    }
    public List<ChoppableItem> ChoppableItems;

    private bool Hovered = false;
    private TintController TintController;
    private SoundManager SoundManager;

    private void Start()
    {
        TintController = GetComponent<TintController>();
        SoundManager = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
    }

    void Update()
    {
        if (ChopHealth <= 0f) ChopDestroy();
        if (Hovered)
        {
            TintController.EnableTint();
            Hovered = false;
        }
        else
        {
            TintController.DisableTint();
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, DropRange);
    }

    public override void Interact() { }

    Vector3 RandomPosition()
    {
        var pos = Random.insideUnitSphere.normalized * DropRange;
        pos.z = 0f;
        return pos;
    }

    void DropItems()
    {
        foreach (var item in ChoppableItems)
        {
            
            for (int i = 0; i < item.EnsuredCount; i++)
                Instantiate(item.Item, transform.position + RandomPosition(), Quaternion.Euler(0f, 0f, 0f));

            for (int i = 0; i < item.ProbableCount; i++)
            {
                if (Random.value <= item.Probability)
                    Instantiate(item.Item, transform.position + RandomPosition(), Quaternion.Euler(0f, 0f, 0f));
            }
        }
    }

    void ChopDestroy()
    {
        Debug.Log("ChopHealth ended");
        DropItems();
        Destroy(gameObject);
    }

    public override void OnIteractibleHovered()
    {
        Hovered = true;
    }

    public override void Interact(KeyValuePair<ItemInfo, int> ItemInHand)
    {
        switch (ChoppableType)
        {
            case ChoppableType.TREE:
                if (ItemInHand.Key.Tags.Axe) Chop();
                break;
            case ChoppableType.ROCK:
                if (ItemInHand.Key.Tags.Pickaxe) Chop();
                break;
            default:
                break;
        }
    }

    private void Chop()
    {
        ChopHealth -= 1f;
        switch (ChoppableType)
        {
            case ChoppableType.TREE:
                SoundManager.PlaySound(SoundType.TREE_CHOP);
                break;
            case ChoppableType.ROCK:
                SoundManager.PlaySound(SoundType.ROCK_CHOP);
                break;
            default:
                break;
        }
    }
}
