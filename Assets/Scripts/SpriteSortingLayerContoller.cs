﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteSortingLayerContoller : MonoBehaviour
{
    private Transform PlayerTransform;
    private SpriteRenderer Renderer;

    void Start()
    {
        PlayerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        Renderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (transform.position.y > PlayerTransform.position.y)
            Renderer.sortingLayerName = "Below Player";
        else Renderer.sortingLayerName = "Over Player";
        Renderer.sortingOrder = (int) (-transform.position.y * 100);
    }
}
