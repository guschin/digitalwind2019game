using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ChunkController : MonoBehaviour
{
    public GameObject FollowObject;
    public float ChunkSideSize;

    private WorldGenerate Generator;

    private void Start()
    {
        Generator = GameObject.FindGameObjectWithTag("WorldGenerator").GetComponent<WorldGenerate>();
    }

    private void Update()
    {
        var position = FollowObject.transform.position;

        int x = (int) (position.x / (ChunkSideSize)), y = (int) (position.y / (ChunkSideSize));
        
        for (int deltax = -2; deltax <= 2; deltax++)
        {
            for (int deltay = -2; deltay <= 2; deltay++)
            {
                var ChunkPos = new Vector3(x + deltax, y + deltay);
                if (!GlobalState.LoadedChunks.Contains(new KeyValuePair<int, int>(x + deltax, y + deltay)))
                {
                    Generator.GenerateChunk(ChunkPos, ChunkSideSize);
                    GlobalState.LoadedChunks.Add(new KeyValuePair<int, int>(x + deltax, y + deltay));
                    Debug.LogFormat("Generated Chunk {0} {1}", ChunkPos.x, ChunkPos.y);
                }
            }
        }
        
    }
}
